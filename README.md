# Scrapper

Scrapper for getting jackpot numbers of the eurolotto lottery for a given year

# Usage
- Add package to your composer.json file:

    ```json

        "repositories": [
            {
                "type": "vcs",
                "url": "https://gitlab.com/kreevit/scrapper.git"
            }
        ],

        "require": {
            "kreevit/scrapper": "@dev"
        }

    ```

- Update packages: `composer update`

- Require autoloader: ` require 'vendor/autoload.php';`

- Get numbers of 2021

    ```php
    use Kreevit\Scrapper\Scrapper;

    $year = 2021;

    $scrapper = Scrapper::getInstance();

    $results = $scrapper->run($year);

    var_dump($results);
    ```
