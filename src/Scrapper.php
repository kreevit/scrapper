<?php

namespace Kreevit\Scrapper;

// Class to get all eurolotto results from a give year

class Scrapper{


    const EUROLOTTO_URL = 'https://www.lotto.net/eurojackpot/results/';

    private $response = '';

    private $year = '';

    // scrapped data
    private $raw = null;

    protected static $instance = null;

    private function __construct(){

    }

    // Singleton pattern
    public static function getInstance() {

        if (!isset(self::$instance)){

            self::$instance = new self;

        }

        return self::$instance;

    }

    public function setYear($year){

        $this->year = $year;

    }

    private function request(){

        // Initialize a connection with cURL (ch = cURL handle, or "channel")
        $ch = curl_init();

        // Set the URL
        curl_setopt($ch, CURLOPT_URL, self::EUROLOTTO_URL.$this->year);

        // Return the response instead of printing it out
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Send the request and store the result in $response
        $this->response = curl_exec($ch);

        curl_close($ch);

    }

    private function scrap(){

        $dom_selector = 'balls';

        $dom = new \DOMDocument();

        libxml_use_internal_errors(true);

        $dom->loadHTML($this->response);

        libxml_use_internal_errors(false);

        $finder = new \DomXPath($dom);

        // get weeks archive
        $this->raw = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $dom_selector ')]");

    }

    private function getNumbersPerWeek(){

        $results = [];

        // 'archive-container'
        foreach ($this->raw as $week_number => $week) {

            if(isset($week->tagName) && $week->tagName == 'ul'){

                foreach($week->childNodes as $b){

                    if(isset($b->tagName) && $b->tagName == 'li'){

                        foreach ($b->childNodes as $span) {

                            if(isset($span->tagName) && $span->tagName == 'span'){

                                foreach ($span->childNodes as $numbers) {

                                    $results[$week_number][] = $numbers->data;

                                }

                            }


                        }

                    }
                }

            }
        }

        return $results;

    }

    // @return array All jackpot numbers in an array sorted per week (desc)
    public function run($year){

        $this->setYear($year);

        $this->request();

        $this->scrap();

        return $this->getNumbersPerWeek();

    }


}


?>
